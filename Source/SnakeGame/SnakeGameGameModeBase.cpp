// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"

void ASnakeGameGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	PlayerLose = false;
	Score = 0;
}
