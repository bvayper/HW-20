// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
	bool PlayerLose = false;

	UPROPERTY(BlueprintReadWrite)
	int32 Score;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
